#! /bin/bash

sudo pkill -f prod.py
sudo pkill -f lightupdate.py

cd /home/pi/ExoLab/
sudo rm -r update
# gotta be able to pass the clone repo to this script... OR do this different!
git clone https://bitbucket.org/magnitudeio/exolab-update.git
sudo mv exolab-update update

cd /home/pi/ExoLab/update
chmod u+x installer.sh
sudo ./installer.sh
