from app import app
import LCD

LCD.start()
LCD.lcd_string("NETWORK READY",LCD.LCD_LINE_1)
LCD.lcd_string(" ",LCD.LCD_LINE_2)
# 9/17/18 - LK
# disabling debug mode - this will cause BAD THINGS 
# as the web app will restart if it senses a 
# change in files (like the Factory Restore running)
app.run(host='0.0.0.0',port=80,debug=False)
