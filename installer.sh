#! /bin/bash

# V35 7/30/20 BL


echo "Time: $(date). v35 Started update" | sudo tee -a /home/pi/ExoLab/update.log
cp libexoledmatrix.so /home/pi/ExoLab
cp lightupdate.py /home/pi/ExoLab/

# V33 10/21/18 LK
# INSTALLER.SH - 9/18/18 - LK V32

echo "Time: $(date). v33 Started update" | sudo tee -a /home/pi/ExoLab/update.log

#get current version
CVN=$(cat /home/pi/ExoLab/version.txt)

#Update-specific commands

if [ "$CVN" -lt 15 ]; then
	rm /home/pi/ExoLab/update_checker.pyc
	fullcred=$(cat /home/pi/ExoLab/cred)
	cred=${fullcred:0:7}
	echo $cred
	sudo sed -i "4s/.*/ssid=ExoLab:$cred/" /etc/hostapd/hostapd.conf

fi

#Regular cp of system scripts

# Still not sure why this is done.  It adds at least 10 seconds to the process.  LK 9/18/18
echo "Time: $(date). ntp stop" | sudo tee -a /home/pi/ExoLab/update.log
sudo /etc/init.d/ntp stop
sudo ntpd -q -g
sudo /etc/init.d/ntp start
echo "Time: $(date). ntp started again" | sudo tee -a /home/pi/ExoLab/update.log

cp /home/pi/ExoLab/data/bufb.csv /home/pi/ExoLab/data/buf.csv

# This update includes updates to flask/run.py and flask/app/views.py
cp -r flask /home/pi/ExoLab/

cp APclean.sh /home/pi/ExoLab/
cp APsetup.sh /home/pi/ExoLab/
cp color.json /home/pi/ExoLab/
cp updateURL.json /home/pi/ExoLab/

#V33 lightupdate.py
# Moved up because of #v35

cp prod.py /home/pi/ExoLab/

#V33 startup.py
cp startup.py /home/pi/ExoLab/

cp update_checker.py /home/pi/ExoLab/

#V33 NEW freespace.py and exolab.conf
cp freespace.py /home/pi/ExoLab/
cp exolab.conf /home/pi/ExoLab/

chmod u+x update.sh
cp update.sh /home/pi/ExoLab/

# NEW FOR V32
# IMAGEUPLOAD SCRIPT
# Adding a replacement imageupload.sh file (removed the old basic Auth credentials in the file)
cp imageupload.sh /home/pi/ExoLab/

# Hotfix for imageupload.sh with Windows line endings
# Really should not need this anymore.. leaving here for now.  
cp /home/pi/ExoLab/imageupload.sh /home/pi/ExoLab/imageupload.sh.backup
sudo tr -d '\r' < /home/pi/ExoLab/imageupload.sh.backup > /home/pi/ExoLab/imageupload.sh

# LK DEV/PROD Switcher Script
cp switchdev.sh /home/pi/ExoLab/

# just make sure all .sh scripts are executable 
sudo chmod +x /home/pi/ExoLab/*.sh


# Hotfix for crontab update_checker problem
echo "Time: $(date). Crontab repair" | sudo tee -a /home/pi/ExoLab/update.log
( sudo crontab -l -u pi | grep -v -E "update_checker" ; echo "0 0 * * * sudo python /home/pi/ExoLab/update_checker.py" ) | crontab - -u pi

# V33 freespace check.  Edit crontab
# IMPORTANT: MUST BE AFTER the crontab edit above for the update checker fix
echo "Time: $(date). Crontab edit for FREESPACE check" | sudo tee -a /home/pi/ExoLab/update.log
(sudo crontab -l -u pi | grep -v '^#'; echo "30 23 * * * sudo python /home/pi/ExoLab/freespace.py") | sort | uniq | crontab - -u pi

# FACTORY RESET FIX
# Hotfix for installing Factory Reset files where they should have been
# Included should be a backup.zip for version 30.  Critical this should NOT have the cred file.  
# See further notes in the repo on how this backup.zip is built.  
echo "Time: $(date). Started deploy of factory file" | sudo tee -a /home/pi/ExoLab/update.log
# Remove the existing backup folder (if there even is one)
sudo rm -rvf /home/pi/backup
# the backup file is encrypted with a password of 314159
sudo unzip -P 314159 backup.zip -d /home/pi/
# seen some odd permissions things in different ExoLab images - so just being overly cautious here
#     It won't harm anything to do this either way.  
sudo chown -R pi:pi /home/pi/backup
sudo chmod +x /home/pi/backup/*.sh  
echo "Time: $(date). Completed factory backup deploy" | sudo tee -a /home/pi/ExoLab/update.log

#Final copy of version:
cp version.txt /home/pi/ExoLab/
echo "Time: $(date). V33 COMPLETED UPDATE: Reboot in 15 seconds" | sudo tee -a /home/pi/ExoLab/update.log
sleep 15
sudo reboot
