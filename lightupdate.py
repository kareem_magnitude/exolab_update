# V33 10/21/18 LK 
#     upload one image at a time from data/picbuf  
import time
import math
import urllib2
import json
from datetime import datetime
import pigpio
from PIL import Image
from shutil import copyfile
import subprocess
import os
import requests
import ctypes
import signal
import sys

os.chdir("/home/pi/ExoLab") # Make sure we are in program home directory, even if started outside

LED_COUNT = 64
#LED_STRIP = ws.WS2811_STRIP_GRB
pic_period=3600 #should be 3600!!!  for testing I usually temp set to 5 mins (300 seconds)

startHour=7;	# Turn lights on at 7 local time


pi=pigpio.pi()
buttonPin=16
pi.set_mode(buttonPin, pigpio.INPUT)


#Format: 0xWWRRGGBB (WW is unused sice these lights don't have white part)
hueDefs={'green':[0x00, 0xFF, 0x00],
               'blue':[0x00, 0x00, 0xFF],
               'red':[0xFF, 0x00, 0x00],
               'white':[0xFF, 0xFF, 0xFF],
               'magenta':[0xFF, 0x00, 0xFF]} #match the colors from the on-device JSON/online json to GRB values for the light library

credfile=open("/home/pi/ExoLab/cred")
cred=credfile.read()[:24]
credtuple=tuple(cred.split(':')) #Get credentials to access the correct light value from the platform.
warningTime=10 #blinks to blink before waiting 1 minute before snapping a photo.

#Ingredients to assemble json to upload images
json_open='\{\\"data\\":['
ts_open='\{\\"timestamp\\":\\"'
ts_close='\\",\\"filename\\":\\"image.jpg\\"\}'
json_close='],\\"upload_file\\":[\\"filename\\"]\} '
picsfolder = "data/pics/"
picbuffolder = "data/picbuf/"

#Use star syntax to unpack tuple for update_display function
def listToArray(theList):
    arr = (ctypes.c_int32*len(theList))(*theList)
    return (arr, len(theList))

#Turn 3 byte color into one int
def colorarrToColor(r, g, b):
    return ( (r & 0xFF) << 16 ) | \
        ( (g & 0xFF) << 8 ) | \
        (b & 0xFF)

def display(strip,rightColor,leftColor): #Display light colors on left and right sides
    pixels = [leftColor if i%8 < 4 else rightColor for i in range(0, LED_COUNT)]
    strip.update_display(*listToArray(pixels))

def settingToValue(settings): #convert json value in hue, photoperiod, and intensity, to a Color
    nowHour = datetime.now().hour
    validHours= range(startHour, startHour+settings['photoperiod']-1)
    intensity=(nowHour in validHours)*settings['intensity']
    hue = hueDefs[settings['color']]
    #Since intensity scales all values equally, we can multiply it against the whole register
    colorArray = [component*intensity/10 for component in hue]
    color = colorarrToColor(colorArray[0],colorArray[1],colorArray[2]) 
    return color

def arrayize(): #get the setting from the platform using the credential, if possible.
    try:
        r=requests.get('https://classroom.magnitude.io/api/experiments/config/', auth=credtuple)
        settings=r.json()['result']
        assert 'mode' in settings
    except: #If not, use the default lighting 
        defaultLight=open("/home/pi/ExoLab/color.json")
        settings=json.loads(defaultLight.read())
    return settings

def setLight(strip):	#set colors according to settings from platform and send it to display to start lights.
    settings=arrayize()
    if settings['mode']=='dual':
            leftColor = settingToValue(settings['leftSettings'])
            rightColor = settingToValue(settings['rightSettings'])
    elif settings['mode']=='single':
            leftColor = settingToValue(settings['settings'])
            rightColor = settingToValue(settings['settings'])
    print str(leftColor) + " " + str(rightColor)
    display(strip, leftColor, rightColor)   

def flash(strip):		#Show only back row of lights as flash for camera
    flashPixels = [0x00FFFFFF]*8 + [0x00000000]*(LED_COUNT-8)
    strip.update_display(*listToArray(flashPixels))

def clear(strip):	#make dark
    darkPixels = [0x00323232]*LED_COUNT
    strip.update_display(*listToArray(darkPixels))

def bright(strip):	#full white
    brightPixels = [0x00FFFFFF]*LED_COUNT
    strip.update_display(*listToArray(brightPixels))

def imageupload():
    
    iso=datetime.utcnow().isoformat()+"000Z"
    print("Taking picture!")	#generate names for image to be taken
    picname = picsfolder+iso + '.jpg'
    picbufname = picbuffolder+iso + '.jpg' 
    subprocess.call(["raspistill", "-co", "10", "-sa", "5","-sh", "15",  "-q", "15","-rot", "180", "-n", "-o", picname ]) #take image, modify color, saturation, sharpness a little
    img=Image.open(picname) #Rotate image after taking to correct for orientation of camera. If this is done before, the image is cropped.
    img=img.rotate(90)
    img.save(picname)
    
    copyname="./"+picname
    copyfile(copyname,picbufname)#Copy image to buffer folder as well

    bufd_imgs=os.listdir(picbuffolder) #Get all images from buffer folder and send each one individually using json structure / api
    # V33 send each file with a separate API call
    print "Files to process:", len(bufd_imgs)
    for fill in bufd_imgs : 
        tslist = [ts_open+fill[:-4]+ts_close]
        tsstr=','.join(tslist)
        targo=picbuffolder+fill
        print "targo=",targo
        compo=json_open+tsstr+json_close    
        uploadCommand='bash imageupload.sh '+compo + cred + ' ' + targo #Use shell script to upload one image using json.
        print uploadCommand
        pipe=subprocess.Popen(uploadCommand, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        out,err=pipe.communicate()
        result=out.decode()
        if "amazonaws" in result: #If successful upload, we expect amazonaws to respond, and we can clear the buffer.            
            print "Success - we'll remove:",targo
            os.remove(targo)
        print result
        log=open("/home/pi/log.log",'a')
        log.write(result)
        log.close()

    
# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    # Intialize the library (must be called once before other functions).
    strip = ctypes.cdll.LoadLibrary('./libexoledmatrix.so')
    strip.init_display()
    setLight(strip) #This command finds and sets the appropriate lighting to start with
    start=0
    while(True):
        if pi.wait_for_edge(buttonPin,pigpio.RISING_EDGE): #If the button is pressed, find and set the lighting immediately.
            time.sleep(.1)
            setLight(strip)
        elapsed = time.time() - start
        if elapsed > pic_period:	#If more than an hour has passed, start blinking, then take and upload a pic
            for i in range(warningTime):
                clear(strip)
                time.sleep(.25)
                bright(strip)
                time.sleep(.25)
            flash(strip)    
            time.sleep(60)
            imageupload()
            start=time.time()
            setLight(strip)	#after uplaod pic, return to normal lighting


